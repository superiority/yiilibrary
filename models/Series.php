<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "series".
 *
 * @property integer $idSeries
 * @property string $Name
 * @property integer $NumberInSeries
 */
class Series extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'series';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NumberInSeries'], 'integer', 'min' => 1],
            [['Name'], 'string', 'max' => 45, 'min' => 1],
            [['Name','NumberInSeries'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idSeries' => 'id',
            'Name' => 'Название',
            'NumberInSeries' => 'Количество книг в серии',
        ];
    }
}
