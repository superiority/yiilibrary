<?php

namespace app\models;

use Yii;
use app\models\Bookinfo;

/**
 * This is the model class for table "book".
 *
 * @property integer $idBook
 * @property string $BookCode
 * @property integer $idBookInfo
 * @property integer $idWriteOff
 * @property integer $idReceipt
 * @property integer $MaxTermInDays
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['BookCode'], 'required'],
            [['idBookInfo', 'idWriteOff', 'idReceipt', 'MaxTermInDays'], 'integer'],
            [['BookCode'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBook' => 'Id Book',
            'BookCode' => 'Book Code',
            'idBookInfo' => 'Id Book Info',
            'idWriteOff' => 'Id Write Off',
            'idReceipt' => 'Id Receipt',
            'MaxTermInDays' => 'Max Term In Days',
        ];
    }

    public function getBookinfo()
    {
        return $this->hasOne(Bookinfo::className(), array('idBookInfo' => 'idBookInfo'));
    }
}
