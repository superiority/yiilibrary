<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publisher".
 *
 * @property integer $idPublisher
 * @property string $Name
 * @property integer $LicenseNumber
 */
class Publisher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publisher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LicenseNumber'], 'integer'],
            [['Name'], 'string', 'max' => 45],
            [['LicenseNumber'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPublisher' => 'Id Publisher',
            'Name' => 'Name',
            'LicenseNumber' => 'License Number',
        ];
    }
}
