<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bookinfo;
use yii\data\Pagination;

/**
 * BookinfoSearch represents the model behind the search form about `app\models\Bookinfo`.
 */
class BookinfoSearch extends Bookinfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idBookInfo', 'idPublisher', 'idSeries', 'Year', 'ISBN'], 'integer'],
            [['Title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bookinfo::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        //$query = Bookinfo::find()->with('publisher')->orderBy('idBookInfo');
        $query = Bookinfo::find()->with('category')->with('author')->orderBy('idBookInfo');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idBookInfo' => $this->idBookInfo,
            'idPublisher' => $this->idPublisher,
            'idSeries' => $this->idSeries,
            'Year' => $this->Year,
            'ISBN' => $this->ISBN,
        ]);

        $query->andFilterWhere(['like', 'Title', $this->Title]);

        return $dataProvider;
    }
}
