<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $idCategory
 * @property string $Name
 * @property integer $idParentCategory
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idParentCategory'], 'integer'],
            [['Name'], 'string', 'max' => 45, 'min' => 1],
            [['idParentCategory'],'compare','compareAttribute' =>
                'idCategory',
                'operator' => '!=',
                'message' => 'Раздел не может быть своим родителем!'],
            [['idParentCategory','Name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCategory' => 'id',
            'Name' => 'Название',
            'idParentCategory' => 'Родительский раздел',
            'parent.Name' => 'Родительский раздел',
        ];
    }

    public function getParent()
    {
        return $this->hasOne($this->className(), array('idCategory' => 'idParentCategory'));
    }
}
