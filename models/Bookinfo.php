<?php

namespace app\models;

use Yii;
use app\models\Publisher;
use app\models\Series;
use app\models\Category;
use app\models\Author;
use yii\debug\models\search\Debug;
use yii\debug\models\search\Log;

/**
 * This is the model class for table "bookinfo".
 *
 * @property integer $idBookInfo
 * @property integer $idPublisher
 * @property integer $idSeries
 * @property string $Title
 * @property integer $Year
 * @property string $ISBN
 */
class Bookinfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookinfo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPublisher', 'idSeries', 'Year'], 'integer'],
            [['Title'], 'string', 'max' => 45],
            [['idPublisher', 'idSeries', 'Year', 'ISBN','Title'/*,'author','category'*/], 'required', 'message' => 'Это обязательное поле!'],
            [['ISBN'], 'match', 'pattern' => '/^[0-9]{13}$/', 'message' => 'ISBN должен быть 13 значным числом.'],//'string', 'min' => 13, 'max' => 13],
            [['Year'], 'integer', 'max' => date('o'), 'min' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBookInfo' => 'Id',
            'idPublisher' => 'Издатель',
            'idSeries' => 'Серия',
            'Title' => 'Название',
            'Year' => 'Год выпуска',
            'ISBN' => 'ISBN',
            'publisher.Name' => 'Издатель',
            'series.Name' => 'Серия',
            //'category.Name' => 'Раздел',
            'author' => 'Автор',
            'category' => 'Раздел',
        ];
    }

    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), array('idPublisher' => 'idPublisher'));
    }

    public function getSeries()
    {
        return $this->hasOne(Series::className(), array('idSeries' => 'idSeries'));
    }

    public function getCategory()
    {
        return $this->hasMany(Category::className(), array('idCategory' => 'idCategory'))->viaTable('bookinfocategories',['idBookInfo' => 'idBookInfo']);
    }

    public function getAuthor()
    {
        return $this->hasMany(Author::className(), array('idAuthor' => 'idAuthor'))->viaTable('bookinfoauthors',['idBookInfo' => 'idBookInfo']);
    }

    public function authorString()
    {
        //$model = $this->find()->with('author')->where("title='$data->Title'")->all()[0]['author'];
        $model =  $this->find()->with('author')->where(['idBookInfo'=>$this->idBookInfo])->one()['author'];

        $cat = '';
        foreach ($model as $temp)
        {
            $cat .= $temp['Name'] . ' ' . $temp['Surname'] . ", ";
        }
        return $cat;
    }

    public function categoryString()
    {
        //$model = $this->find()->with('category')->where("title='$data->Title'")->all()[0]['category'];
        $model = $this->find()->with('category')->where(['idBookInfo'=>$this->idBookInfo])->one()['category'];

        $cat = '';
        foreach ($model as $temp)
        {
            $cat .= $temp['Name'] . ", ";
        }
        return $cat;
    }

    public function tempSave()
    {
        //Добавляем авторов
        $author = Yii::$app->request->post('Bookinfo')['author'];
        if (!is_array($author))
            $author[] = 0;
        $sql = "DELETE FROM `bookinfoauthors` WHERE `idBookInfo` = $this->idBookInfo";
        \Yii::$app->db->createCommand($sql)->execute();
        foreach($author as $auth)
        {
            $sql = "INSERT INTO `bookinfoauthors`(`idBookInfo`, `idAuthor`) VALUES ($this->idBookInfo,$auth)";
            \Yii::$app->db->createCommand($sql)->execute();
        }

        //Добавляем разделы
        $categories = Yii::$app->request->post('Bookinfo')['category'];
        if (!is_array($categories))
            $categories[] = 0;
        $sql = "DELETE FROM `bookinfocategories` WHERE `idBookInfo` = $this->idBookInfo";
        \Yii::$app->db->createCommand($sql)->execute();
        foreach($categories as $cat)
        {
            $sql = "INSERT INTO `bookinfocategories`(`idBookInfo`, `idCategory`) VALUES ($this->idBookInfo,$cat)";
            \Yii::$app->db->createCommand($sql)->execute();
        }
    }

}
