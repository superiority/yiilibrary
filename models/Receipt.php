<?php

namespace app\models;

use Yii;
use app\models\Book;

/**
 * This is the model class for table "receipt".
 *
 * @property integer $idReceipt
 * @property string $Date
 * @property string $Description
 */
class Receipt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'receipt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Date'], 'date','message'=> 'Дата не может быть позже сегодняшней.', 'format' => 'yyyy-MM-dd', 'max' => date('o-m-d')],
            [['Description'], 'string', 'max' => 150],
            [['Date'], 'required', 'message' => 'Это обязательное поле!'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idReceipt' => 'id',
            'Date' => 'Дата',
            'Description' => 'Описание',
        ];
    }

    public function getBooks()
    {
        return $this->hasMany(Book::className(), array('idReceipt' => 'idReceipt'));
    }
}
