<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookinfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Наименования';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookinfo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout' => '{items}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'Title',
            [
                'value'=> function($data){
                    $cat = '';
                    foreach ($data->author as $temp)
                    {
                        $cat .= $temp['Name'] . ' ' . $temp['Surname'] . "<br>";
                    }
                    return $cat;
                },
                'format' => 'raw',
                'header' => 'Автор',
            ],
            [
                'value' => 'Year',
                'options' => ['width' => '4%'],
                'header' => 'Год'
            ],
            'series.Name',
            [
                'value'=> function($data){
                    $cat = '';
                    foreach ($data->category as $temp)
                    {
                        $cat .= $temp['Name'] . "<br>";
                    }
                    return $cat;
                },
                'format' => 'raw',
                'header' => 'Раздел',
            ],
            'publisher.Name',
            'ISBN',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p class="pull-right">
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]) ?>
</div>
