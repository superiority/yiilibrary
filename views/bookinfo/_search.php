<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BookinfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bookinfo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idBookInfo') ?>

    <?= $form->field($model, 'idPublisher') ?>

    <?= $form->field($model, 'idSeries') ?>

    <?= $form->field($model, 'Title') ?>

    <?= $form->field($model, 'Year') ?>

    <?php // echo $form->field($model, 'ISBN') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
