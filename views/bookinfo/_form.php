<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Publisher;

/* @var $this yii\web\View */
/* @var $model app\models\Bookinfo */
/* @var $form yii\widgets\ActiveForm */
$publishers = array();
$series = array();
$authors = array();
$categories = array();
?>

<div class="bookinfo-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php
    $temp = Publisher::find()->all();
    $publishers = \yii\helpers\ArrayHelper::map($temp,'idPublisher','Name');
    $publishers[0] = 'Отсутствует';

    $temp = \app\models\Series::find()->all();
    $series = \yii\helpers\ArrayHelper::map($temp,'idSeries','Name');
    $series[0] = "Отсутствует";

    $temp = \app\models\Author::find()->all();
    foreach($temp as $key => $value)
    {
        $value['Name'] .= ' ' . $temp[$key]['Surname'];
    }
    $authors = \yii\helpers\ArrayHelper::map($temp,'idAuthor','Name');
    $authors[0] = "Отсутствует";

    $temp = \app\models\Category::find()->all();
    $categories = \yii\helpers\ArrayHelper::map($temp,'idCategory','Name');
    $categories[0] = "Отсутствует";
    ?>

    <?= $form->field($model, 'Title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'author')->dropDownList($authors,['multiple' => true]); ?>
    <?= $form->field($model, 'Year')->textInput() ?>
    <?= $form->field($model, 'idSeries')->dropDownList($series); ?>
    <?= $form->field($model, 'category')->dropDownList($categories, ['multiple' => true]); ?>
    <?= $form->field($model, 'idPublisher')->dropDownList($publishers); ?>
    <?= $form->field($model, 'ISBN')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
