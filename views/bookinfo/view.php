<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Bookinfo */

$this->title = $model->Title;
$this->params['breadcrumbs'][] = ['label' => 'Наименования', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookinfo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idBookInfo',
            'Title',
            [
                'label' => 'Автор',
                'attribute' => 'author',
                'value' => $model->authorString(),//$model->authorString($model),
            ],
            'Year',
            'series.Name',
            [
                'label' => 'Раздел',
                'attribute' => 'category.Name',
                'value' => $model->categoryString(),//$model->categoryString($model),
            ],
            'publisher.Name',
            'ISBN',
        ],
    ]) ?>

    <p class="pull-right">
        <?= Html::a('Изменить', ['update', 'id' => $model->idBookInfo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->idBookInfo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить это?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
