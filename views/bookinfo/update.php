<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bookinfo */

$this->title = 'Изменить наименование: ' . ' ' . $model->Title;
$this->params['breadcrumbs'][] = ['label' => 'Наименование', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Title, 'url' => ['view', 'id' => $model->idBookInfo]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="bookinfo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
