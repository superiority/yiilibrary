<?php
/* @var $this yii\web\View
 * @var $receipts array
 * @var $pagination Pagination
 * @var $books array
 */

use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\data\Pagination;

$this->title = 'Поступления';
$this->params['breadcrumbs'][] = $this->title;
$disp = Yii::$app->request->get('disp');
?>
<table class="table table-striped">
    <thead>
    <tr>
        <td><b>Описание</b></td>
        <td><b>Дата</b></td>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($receipts as $receipt) {
        echo "<tr>
                <td>{$receipt['Description']}</td>
                <td>{$receipt['Date']}</td>";
        if ($receipt['idReceipt'] == $disp)
        {
            echo "<td class='text-right'>" . Html::a('Закрыть',['index','disp' => -1,'page'=>$pagination->getPage() + 1],['class' => ['btn btn-default btn-sm']]) . "</td>
            </tr>";
            display($books);
        }
        else
            echo "<td class='text-right'>" . Html::a('Просмотреть',['index','disp' => $receipt['idReceipt'],'page'=>$pagination->getPage() + 1],['class' => ['btn btn-default btn-sm']]) . "</td>
            </tr>";
    }
    ?>
    </tbody>
</table>
<p class="pull-right">
    <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-primary']) ?>
</p>
<?= LinkPager::widget(['pagination' => $pagination]) ?>
<?php

function display($books)
{
    echo "<tr>
            <td colspan='4'>
           <table class='table table-bordered table-condensed '>
            <thead>
                <tr>
                    <th>Инвентарный номер</th>
                    <th>Название</th>
                    <th>Автор</th>
                </tr>
            </thead>
            <tbody>";

    foreach ($books as $book)
    {
        echo "<tr>
                <td>{$book['BookCode']}</td>
                <td>{$book['Title']}</td>
                <td>";
        foreach($book["author"] as $author)
            echo "<div>$author</div>";
        echo "  </td>
             </tr>";
    }
    echo "</tbody>
        </table>
        </td>
       </tr>";
}
?>

