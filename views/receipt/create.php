<?php
/**
 * Created by PhpStorm.
 * User: Feodor
 * Date: 03.01.2016
 * Time: 15:55
 */
/* @var $model app\models\Receipt */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

$this->title = 'Новое поступление';
$this->params['breadcrumbs'][] = ['label' => 'Поступления', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="receipt-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Description')->textInput(['maxlength' => true, 'class' => ['form-control','description']]) ?>

    <?= $form->field($model,'Date')->widget(DatePicker::className(),[
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>




    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
