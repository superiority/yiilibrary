<?php
/**
 * @var $authors array
 * @var $pagination Pagination
 */
use yii\widgets\LinkPager;

$this->title = 'Автора';
$this->params['breadcrumbs'][] = $this->title;
?>
<table class="table table-striped">
    <thead>
    <tr>
        <td><b>Имя</b></td>
        <td><b>Фамилия</b></td>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($authors as $author) {
        echo "<tr><td>" . "{$author['Name']}</td><td>{$author['Surname']}" . "</td></tr>";
    }
    ?>
    </tbody>
</table>
    <?= LinkPager::widget(['pagination' => $pagination]) ?>
