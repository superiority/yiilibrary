<?php
/**
 * @var $publishers array
 * @var $pagination Pagination
 */
use yii\widgets\LinkPager;

$this->title = 'Издатели';
$this->params['breadcrumbs'][] = $this->title;
?>
<table class="table table-striped">
    <thead>
    <tr>
        <td><b>Название</b></td>
        <td><b>Номер лицензии</b></td>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($publishers as $publisher) {
        echo "<tr><td>" . "{$publisher['Name']}</td><td>{$publisher['LicenseNumber']}" . "</td></tr>";
    }
    ?>
    </tbody>
</table>
    <?= LinkPager::widget(['pagination' => $pagination]) ?>
