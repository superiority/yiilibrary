<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Series */

$this->title = $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Серии книг', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="series-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idSeries',
            'Name',
            'NumberInSeries',
        ],
    ]) ?>

    <p class="pull-right">
        <?= Html::a('Изменить', ['update', 'id' => $model->idSeries], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->idSeries], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите это удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
