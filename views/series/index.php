<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SeriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Серии книг';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="series-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idSeries',
            'Name',
            'NumberInSeries',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?= Html::a('Новая серия', ['create'], ['class' => 'btn btn-primary pull-right']) ?>
    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]) ?>
</div>
