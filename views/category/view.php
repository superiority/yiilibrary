<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->Name;
$this->params['breadcrumbs'][] = ['label' => 'Разделы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCategory',
            'Name',
            //'idParentCategory',
            'parent.Name'
        ],
    ]) ?>
    <p class="pull-right">
        <?= Html::a('Изменить', ['update', 'id' => $model->idCategory], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->idCategory], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите это удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
