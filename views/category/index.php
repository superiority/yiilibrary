<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Разделы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo "<pre>"; print_r($searchModel); //echo $this->render('_search', ['model' => $searchModel]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout' => "{items}\n",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idCategory',
            'Name',
            //'idParentCategory',
            'parent.Name',

            ['class' => 'yii\grid\ActionColumn'],
        ],

    ]); ?>
    <p>
        <?= Html::a('Создать раздел', ['create'], ['class' => 'btn btn-primary pull-right']) ?>
    </p>
    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]) ?>
</div>

