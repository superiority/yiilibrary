<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
$cat = array();
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <!--<? $form->field($model, 'idParentCategory')->textInput() ?>-->

    <?php
    $categories = Category::find()->all();
    $cat = \yii\helpers\ArrayHelper::map($categories,'idCategory','Name');
    $cat[0] = 'Отсутствует';
    ?>
    <?= $form->field($model, 'idParentCategory')->dropDownList($cat); ?>

    <?php
    //echo "<pre>";
    //print_r($model);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
