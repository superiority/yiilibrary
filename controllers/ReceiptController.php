<?php

namespace app\controllers;

use Yii;
use app\models\Receipt;
use yii\data\Pagination;
use yii\db\Query;

class ReceiptController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = Receipt::find();
        $books = array();
        $disp = Yii::$app->request->get('disp',-1);

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $receipts = $query->orderBy('idReceipt')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $query = new Query;
        $query  ->select('book.BookCode, bookInfo.Title, author.Name, author.Surname')
                ->from('book')
                ->leftJoin('bookInfo','book.idBookInfo=bookInfo.idBookInfo')
                ->leftJoin('bookinfoauthors','book.idBookInfo=bookinfoauthors.idBookInfo')
                ->leftJoin('author','author.idAuthor=bookinfoauthors.idAuthor')
                ->where("book.idReceipt=$disp");

        if ($disp != -1) {
            $temp = $query->all();

            foreach($temp as $book)
            {
                if (!isset($books[$book['BookCode']]))
                    $books[$book['BookCode']] = [ 'BookCode' => $book['BookCode'],'Title' => $book['Title'], 'author' =>  ["{$book['Name']} {$book['Surname']}"]];
                else
                    $books[$book['BookCode']]['author'][] = "{$book['Name']} {$book['Surname']}";
            }
        }

        return $this->render('index', [
            'receipts' => $receipts,
            'pagination' => $pagination,
            'books' => $books,
        ]);
    }

    public function actionCreate()
    {
        $model = new Receipt();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index',[]]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

}
