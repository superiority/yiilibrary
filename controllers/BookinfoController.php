<?php

namespace app\controllers;

use Yii;
use app\models\Bookinfo;
use app\models\BookinfoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\QueryBuilder;

/**
 * BookinfoController implements the CRUD actions for Bookinfo model.
 */
class BookinfoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bookinfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookinfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bookinfo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bookinfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bookinfo();

        $post = Yii::$app->request->post();
        unset($post['Bookinfo']['author']);
        unset($post['Bookinfo']['category']);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idBookInfo]);
        }*/
        if ($model->load($post) && $model->save()) {

            $model->tempSave();
            /*
            //��������� �������
            $author = Yii::$app->request->post('Bookinfo')['author'];
            if (!is_array($author))
                $author[] = 0;
            $sql = "DELETE FROM `bookinfoauthors` WHERE `idBookInfo` = $model->idBookInfo";
            \Yii::$app->db->createCommand($sql)->execute();
            foreach($author as $auth)
            {
                $sql = "INSERT INTO `bookinfoauthors`(`idBookInfo`, `idAuthor`) VALUES ($model->idBookInfo,$auth)";
                \Yii::$app->db->createCommand($sql)->execute();
            }

            //��������� �������
            $categories = Yii::$app->request->post('Bookinfo')['category'];
            if (!is_array($categories))
                $categories[] = 0;
            $sql = "DELETE FROM `bookinfocategories` WHERE `idBookInfo` = $model->idBookInfo";
            \Yii::$app->db->createCommand($sql)->execute();
            foreach($categories as $cat)
            {
                $sql = "INSERT INTO `bookinfocategories`(`idBookInfo`, `idCategory`) VALUES ($model->idBookInfo,$cat)";
                \Yii::$app->db->createCommand($sql)->execute();
            }
            */

            return $this->redirect(['view', 'id' => $model->idBookInfo]);
        }
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Bookinfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $post = Yii::$app->request->post();
        unset($post['Bookinfo']['author']);
        unset($post['Bookinfo']['category']);

        $model = $this->findModel($id);

        if ($model->load($post) && $model->save()) {

            $model->tempSave();
            /*
            //��������� �������
            $author = Yii::$app->request->post('Bookinfo')['author'];
            if (!is_array($author))
                $author[] = 0;
            $sql = "DELETE FROM `bookinfoauthors` WHERE `idBookInfo` = $model->idBookInfo";
            \Yii::$app->db->createCommand($sql)->execute();
            foreach($author as $auth)
            {
                $sql = "INSERT INTO `bookinfoauthors`(`idBookInfo`, `idAuthor`) VALUES ($model->idBookInfo,$auth)";
                \Yii::$app->db->createCommand($sql)->execute();
            }

            //��������� �������
            $categories = Yii::$app->request->post('Bookinfo')['category'];
            if (!is_array($categories))
                $categories[] = 0;
            $sql = "DELETE FROM `bookinfocategories` WHERE `idBookInfo` = $model->idBookInfo";
            \Yii::$app->db->createCommand($sql)->execute();
            foreach($categories as $cat)
            {
                $sql = "INSERT INTO `bookinfocategories`(`idBookInfo`, `idCategory`) VALUES ($model->idBookInfo,$cat)";
                \Yii::$app->db->createCommand($sql)->execute();
            }
            */
            return $this->redirect(['view', 'id' => $model->idBookInfo]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Bookinfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bookinfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bookinfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bookinfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
