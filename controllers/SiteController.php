<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use app\models\Author;
use app\models\Publisher;

/*
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;
*/
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAuthors()
    {
        $query = Author::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $authors = $query->orderBy('idAuthor')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('authors', [
            'authors' => $authors,
            'pagination' => $pagination,
        ]);
    }

    public function actionPublishers()
    {
        $query = Publisher::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $publishers = $query->orderBy('idPublisher')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('publishers', [
            'publishers' => $publishers,
            'pagination' => $pagination,
        ]);
    }
}
